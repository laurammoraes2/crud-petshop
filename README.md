 ![SoftMakers](https://www.softmakers.com.br/assets/img/logotipo14xxhdpi.png)

# Desafio - Desenvolvedor Fullstack Júnior
Seja bem-vindo! Este desafio foi projetado para avaliar a sua capacidade técnica como candidato à vaga de Desenvolvedor Fullstack Júnior.

## Instruções para rodar a aplicação 

- Instale as dependências: npm install 
- Rode o comando: npm run start 

## Como foi realizado o desenvolvimento da aplicação?

No desenvolvimento da plataforma de PetShop utilizei HTML, CSS, JavaScript, NodeJS e Mongodb como banco de dados para armazenar as informações. 
Para rederização das views utilizei engine EJS. 

